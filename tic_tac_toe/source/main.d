import raylib;
import std.stdio;
import std.format;
import std.string;
const int SCREENWIDTH = 800;
const int SCREENHEIGHT = 800;
string GAMESTATE = "CROSS";

void switchGameState()
{
  if (GAMESTATE == "CROSS")
    GAMESTATE = "CIRCLE";
  else
    GAMESTATE = "CROSS";
}

struct Tile {
  Rectangle rec;
  string letter = " ";
  Colors color = Colors.BLACK;
  this(Rectangle rec)
  {
    this.rec = rec;
  }

  void draw()
  {
    if (letter == " ")
      {
	return;
      }
    // +50 looks good to me!
    DrawText(letter.ptr, cast(int)rec.x + 50, cast(int)rec.y, 250, color);
  }
  //!! figure out why the letter isn't drawing
  void mouseColliding(Rectangle mouseRect){
    if (CheckCollisionRecs(mouseRect, this.rec))
      {
	if (IsMouseButtonPressed(MouseButton.MOUSE_LEFT_BUTTON))
	  {
	    if (GAMESTATE == "CROSS")
	      {
		if (this.letter == "X" || this.letter == "O")
		  draw();
		else
		  {
		    writeln("Drawing Cross");
		    this.letter = "X";
		    draw();
		    switchGameState();
		  }

	      }
	    else
	      {
		if (this.letter == "X" || this.letter == "O")
		  draw();
		else
		  {
		    writeln("Drawing Circle");
		    this.letter = "O";
		    draw();
		    switchGameState();
		  }
	      }
	  }
      }
  }
}


Tile[3][3] CreateGrid(const int screenWidth, const int screenHeight, int offset = 10)
{
  int borderThickness = 5;
  int tilePadding = (2 * borderThickness) + offset;
  Tile[3][3] tileArray;
  int padding = 5;
  int tilePosX;
  int tilePosY;
  int tilePos;
  int tileSize = 250;

  foreach(int i; 0..tileArray.length)
    foreach(int j; 0..tileArray.length)
      {
	if (i == 0 && j == 0)
	  {
	    tilePosX = offset + borderThickness + padding;
	    tilePosY = offset + borderThickness + padding;
	    
	    tileArray[i][j] = Tile(Rectangle(tilePosX, tilePosY, tileSize, tileSize));
	  }
	else if (i == 1 && j == 0)
	  {
	    tilePosX = offset + borderThickness + padding;
	    tilePosY = cast(int)tileArray[i-1][0].rec.y + padding + tileSize;
	    tileArray[i][j] = Tile(Rectangle(tilePosX, tilePosY, tileSize, tileSize));
	  }
	else if (i == 2 && j == 0)
	  {	
	    tilePosX = offset + borderThickness + padding;
	    tilePosY = cast(int)tileArray[i-1][0].rec.y + padding + tileSize;
	    tileArray[i][j] = Tile(Rectangle(tilePosX, tilePosY, tileSize, tileSize));
	  }	
	else
	  {
	    tilePosX = cast(int)tileArray[i][j-1].rec.x + padding + tileSize;
	    tilePosY = cast(int)tileArray[i][j-1].rec.y;
	    tileArray[i][j] = Tile(Rectangle(tilePosX, tilePosY, tileSize, tileSize));
	  }
      }
  return tileArray;

}

void DrawGrid(Tile[3][3] arr)
{
  foreach(int i; 0..arr.length)
    foreach(int j; 0..arr.length){
      {
	DrawRectangleLinesEx(arr[i][j].rec, 5, Colors.BLACK);
        arr[i][j].draw();
      }  
    }
}

void CheckCollisions(ref Tile[3][3] arr, Rectangle mouseRect)
{
  foreach(int i; 0..arr.length)
    foreach(int j; 0..arr.length){
      arr[i][j].mouseColliding(mouseRect);
    }
}

void DebugRectangles(Tile[3][3] arr, Rectangle mouseRect)
{
  foreach(int i; 0..arr.length)
    foreach(int j; 0..arr.length){
      DrawRectangleRec(arr[i][j].rec, Colors.RED);
    }
  DrawRectangleRec(mouseRect, Colors.BLUE);  
}

void CheckWinner(Tile[3][3] arr){
  // check top row
  //writeln("", arr[0][0].letter , arr[0][1].letter && arr[0][1].letter , arr[0][2].letter);
  //auto s = ;
  if (arr[0][0].letter != " " && arr[0][0].letter == arr[0][1].letter && arr[0][1].letter == arr[0][2].letter){
    DrawText(format("%s Wins!", arr[0][0].letter).ptr, SCREENWIDTH / 4, SCREENHEIGHT / 4, 120, Colors.BLUE);
    GAMESTATE = "OVER";
  }
  // check mid row
  else if (arr[1][0].letter != " " && arr[1][0].letter == arr[1][1].letter && arr[1][1].letter == arr[1][2].letter){
    DrawText(format("%s Wins!", arr[1][0].letter).ptr, SCREENWIDTH / 4, SCREENHEIGHT / 4, 120, Colors.BLUE);
    GAMESTATE = "OVER";
  }
  // check bot row
  else if (arr[2][0].letter != " " && arr[2][0].letter == arr[2][1].letter && arr[2][1].letter == arr[2][2].letter){
    DrawText(format("%s Wins!", arr[2][0].letter).ptr, SCREENWIDTH / 4, SCREENHEIGHT / 4, 120, Colors.BLUE);
    GAMESTATE = "OVER";
  }
  // check vert left
  else if (arr[0][0].letter != " " && arr[0][0].letter == arr[1][0].letter && arr[1][0].letter == arr[2][0].letter){
    DrawText(format("%s Wins!", arr[0][0].letter).ptr, SCREENWIDTH / 4, SCREENHEIGHT / 4, 120, Colors.BLUE);
    GAMESTATE = "OVER";
  }
  // check vert mid
  else if (arr[0][1].letter != " " && arr[0][1].letter == arr[1][1].letter && arr[1][1].letter == arr[2][1].letter){
    DrawText(format("%s Wins!", arr[0][1].letter).ptr, SCREENWIDTH / 4, SCREENHEIGHT / 4, 120, Colors.BLUE);
    GAMESTATE = "OVER";
  }
  // check vert mid
  else if (arr[0][2].letter != " " && arr[0][2].letter == arr[1][2].letter && arr[1][2].letter == arr[2][2].letter){
    DrawText(format("%s Wins!", arr[0][2].letter).ptr, SCREENWIDTH / 4, SCREENHEIGHT / 4, 120, Colors.BLUE);
    GAMESTATE = "OVER";
  }
  // top left diag
  else if (arr[0][0].letter != " " && arr[0][0].letter == arr[1][1].letter && arr[1][1].letter == arr[2][2].letter){
    DrawText(format("%s Wins!", arr[0][0].letter).ptr, SCREENWIDTH / 4, SCREENHEIGHT / 4, 120, Colors.BLUE);
    GAMESTATE = "OVER";
  }
  // top right diag
  else if (arr[0][2].letter != " " && arr[0][2].letter == arr[1][1].letter && arr[1][1].letter == arr[2][0].letter){
    DrawText(format("%s Wins!", arr[0][2].letter).ptr, SCREENWIDTH / 4, SCREENHEIGHT / 4, 120, Colors.BLUE);
    GAMESTATE = "OVER";
  }

}

void main(){
  InitWindow(SCREENWIDTH, SCREENHEIGHT, "Tic Tac Toe");
  Vector2 mousePos = GetMousePosition();
  Rectangle mouseRect = Rectangle(mousePos.x, mousePos.y, 10, 10);
  Tile[3][3] tileArray = CreateGrid(SCREENWIDTH,SCREENHEIGHT);

  while (GAMESTATE != "QUIT")
    {

      mousePos = GetMousePosition();
      mouseRect = Rectangle(mousePos.x, mousePos.y, 10, 10);
      BeginDrawing();
      //DebugRectangles(tileArray, mouseRect);      
      ClearBackground(Colors.RAYWHITE);
      DrawGrid(tileArray);
      CheckCollisions(tileArray,mouseRect);
      CheckWinner(tileArray);
      EndDrawing();
      if (GAMESTATE == "OVER"){
	
	write("Would you like to play again? (y/n) ");
	string quit = strip(readln());
	writeln("GAMESTATE IS = ", GAMESTATE);
	
	if (quit == "no" || quit == "n")
	  writeln("the value of quit is ", quit, "d");
	  
	  GAMESTATE = "QUIT";
      }      

    }

 
}
