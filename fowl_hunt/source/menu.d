module menu;

import raylib;
import data;
import std.stdio;

class Menu: Istate{
  GameState gstate = GameState.MENU;
  
  this() {}
  GameState Run() {
    while(gstate == GameState.MENU){
      BeginDrawing();
      DrawText("FOWL", SCREENWIDTH / 2 - 150, SCREENHEIGHT / 4 - 100, 100, Colors.RED);
      DrawLineEx(Vector2(200,200), Vector2(650, 200), 10,  Colors.RED);
      DrawText("HUNT", SCREENWIDTH / 2 - 50, SCREENHEIGHT / 4, 100, Colors.RED);
      DrawText("GAME A\t1 FOWL", SCREENWIDTH / 2 - 100, SCREENHEIGHT / 4 + 100, 40, Colors.RED);
      DrawText("GAME B\t2 FOWL", SCREENWIDTH / 2 - 100, SCREENHEIGHT / 4 + 150, 40, Colors.RED);
      DrawText("GAME C\t2 CLAY SHOOTING", SCREENWIDTH / 2 - 110, SCREENHEIGHT / 4 + 200, 40, Colors.RED);      
      DrawText("TOP SCORE = 100", SCREENWIDTH / 2 - 100, 700, 40, Colors.RED);
      ClearBackground(Colors.RAYWHITE);      
      EndDrawing();
      HandleEvents();
    }
    return gstate;
  }

  void HandleEvents(){
    if (WindowShouldClose()){
      gstate = GameState.EXIT;
    } else if (IsKeyPressed(KeyboardKey.KEY_SPACE)){
      gstate = GameState.PLAY;
    }
    
  }
}
