module data;
import raylib;
import std.stdio;
import std.math: round;
import std.algorithm: remove;

enum GameState { MENU, PLAY, EXIT }
const int SCREENWIDTH = 800;
const int SCREENHEIGHT = 800;
string SCORE = "000000";

interface Istate {
  
  GameState Run();
}

class Entity{
  Image img;
  Texture2D tex;
  Vector2 pos;
  int width;
  int height;
  Colors color = Colors.WHITE; // set by default

  this(Image img){
    this.img = img;
    this.tex = LoadTextureFromImage(this.img);
    this.width = img.width;
    this.height = img.height;
  }

  abstract void Update();

  abstract void Draw();

  abstract void DrawCollision();

  void ScaleImage(double width, double height){
    this.width =  cast(int)round(cast(double)this.width * width) ;
    this.height = cast(int)round(cast(double)this.height * height) ;
    ImageResize(&this.img, this.width, this.height);
    this.tex = LoadTextureFromImage(this.img);
  }
}

class Fowl : Entity{
  Rectangle rec;
  int scale = 2;
  enum AnimState {ALIVE, SHOT, DEAD};
  AnimState state = AnimState.ALIVE;
  int frameCounter = 0;
  int frameSpeed = 8;
  int currentFrame = 0;
  static Fowl[] Fowls;

  this(Image img){
    super(img);
    this.pos = Vector2(SCREENWIDTH / 2, SCREENHEIGHT / 2);
    ScaleImage(scale,scale);
    this.rec = Rectangle(this.pos.x , this.pos.y, this.width / 8, this.height);
    Fowls ~= this;
    // this.rec = Rectangle(this.pos.x - (this.width / 2), this.pos.y - (this.height  / 2), this.width, this.height) ;    
  }

  override void Update(){
    Draw();    
  }

  override static void Draw(){
 DrawCollision();

 if (state == AnimState.ALIVE){
   if (frameCounter >= (60/frameSpeed)){
     currentFrame = ++currentFrame % 2;
     DrawTextureRec(
		    this.tex,
		    Rectangle(currentFrame * this.width / 8, 0, tex.width / 8,  this.height),
		    Vector2(SCREENWIDTH/2, SCREENHEIGHT/2),
		    Colors.WHITE);
     frameCounter=0;
 
   }else{
     DrawTextureRec(
		    this.tex,
		    Rectangle(currentFrame * this.width / 8, 0, tex.width / 8,  this.height),
		    Vector2(SCREENWIDTH/2, SCREENHEIGHT/2),
		    Colors.WHITE);
     frameCounter++;
   }
 }
   


  }

  override void DrawCollision(){
   DrawRectangleRec(rec, Colors.RED);
  }

  void Kill(){
    writeln("You Shot me!");
    UnloadTexture(this.tex);
  }
}

class Crosshair : Entity{
  int bullets = 3;
  int reloads = 1;
  this(Image img_crosshair){
    super(img_crosshair);
    this.pos = GetMousePosition();
    HideCursor(); // No longer needed with crosshair
  }

  override void Update(){
    pos = GetMousePosition();
    Draw();
  }

  override void Draw(){
    DrawTexture(tex, cast(int)pos.x - (width / 2), cast(int)pos.y - (height / 2), color);
  }

  override void DrawCollision(){
    // draw circle for collision that is the size of the interior of the image
    DrawCircle(cast(int)pos.x, cast(int)pos.y, (this.width / 2) - 6, Colors.RED);
  }

  Fowl[] CheckCollisions(Fowl[] fowl){
    Fowl[] hits;
    
    foreach(fwl; fowl){
      //bool CheckCollisionCircleRec(Vector2 center, float radius, Rectangle rec);
      if (CheckCollisionCircleRec(Vector2(cast(int)pos.x, cast(int)pos.y), (this.width / 2) - 6, fwl.rec))
	writeln("You collided");
	hits ~= fwl;
    }

    return hits;
    
    
  }


  void Reload(){
    if (reloads != 0 && bullets == 0){
      reloads -= 1;
      bullets = 3;
      writeln("Reloading");
    }
  }

  void Shoot(Fowl[] fowl){
    if (bullets != 0){
      bullets -= 1;
      if(CheckCollisions(fowl)){
	foreach (idx, fwl; fowl){
	  fwl.Kill();
	  fwl.Fowls = remove(fwl.Fowls, idx);
	  //destroy(fwl);
	}
	// for (int i = 0; i < fowl.length; i++){
	//   fowl[i].Kill();
	//   fowl[i] =
	  
	// }
      }
    }
    else{
      writeln("Out of bullets");
    }
  }
}
