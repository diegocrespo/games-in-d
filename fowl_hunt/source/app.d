import std.stdio;
import raylib;
import data;
import ui;
import menu;
import play;

class ContextManager {
  GameState gstate = GameState.MENU;
  Menu menu = new Menu();
  
  this(){
    InitWindow(SCREENWIDTH, SCREENHEIGHT, "Fowl Hunt");
    SetTargetFPS(60);
    this.execute();
  }

  void execute() {
    while(gstate != GameState.EXIT){
      if (gstate == GameState.MENU){
	gstate = menu.Run();
      }else if (gstate == GameState.PLAY){
	Play play = new Play();
	gstate = play.Run();
      }
    }
  }
  ~this() {
    CloseWindow();
  }
}


void main()
{
  ContextManager c = new ContextManager();
}
 
