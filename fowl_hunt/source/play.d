module play;

import raylib;
import data;
import ui;
import std.stdio;

class Play : Istate{
  GameState gstate = GameState.PLAY;
  Crosshair crosshair;
  Fowl fowl;
  Label shot;
  Label hit;
  Label score;
  Label scoreText;
  Label reload;
  Image background;
  Image bulletImg1;
  Image bulletImg2; 
  Image bulletImg3;
  Rectangle shot_rect;  
  Rectangle reload_rect;
  Rectangle score_rect;  
  Rectangle hit_rect;  
  this(){
    crosshair = new Crosshair(LoadImage("crosshair.png"));
    bulletImg1 = LoadImage("bullet.png");
    ImageResize(&bulletImg1, 15, 32);
    bulletImg2 = bulletImg1;
    bulletImg3 = bulletImg1;
    fowl = new Fowl(LoadImage("fowl_sprite_sheet_final.png"));
    shot = new Label(80, 745, 25, "SHOT", Colors.WHITE);
    hit = new Label(205, 710, 40, "HIT", Colors.GREEN);
    score = new Label(605, 710, 40, SCORE, Colors.WHITE);
    scoreText = new Label(615, 740, 38, "SCORE", Colors.WHITE);
    reload = new Label(80, 655, 38, "R=3", Colors.BLUE);
    score_rect = Rectangle(592,705, 162, 75);
    shot_rect = Rectangle(65,705, 90, 75);
    reload_rect = Rectangle(70, 655, 85, 40);
    hit_rect = Rectangle(195,705, 365, 75);
    background = LoadImage("fowl.png");
  }
  GameState Run(){
    while(gstate == GameState.PLAY){
      BeginDrawing();
      DrawTexture(LoadTextureFromImage(background), 0, 0,Colors.WHITE);
      DrawRectangleRounded(shot_rect, .2, 0, Colors.BLACK);
      DrawRectangleRounded(score_rect, .2, 0, Colors.BLACK);
      DrawRectangleRounded(reload_rect, .2, 0, Colors.BLACK);           
      DrawTexture(LoadTextureFromImage(bulletImg1),80, 712, Colors.WHITE);
      DrawTexture(LoadTextureFromImage(bulletImg2),105, 712, Colors.WHITE);
      DrawTexture(LoadTextureFromImage(bulletImg3),130, 712, Colors.WHITE);      
      shot.Update();
      DrawRectangleRounded(hit_rect, .2, 0,Colors.BLACK);      
      hit.Update();
      scoreText.Update();
      score.Update();
      fowl.Update();
      reload.Update();
      crosshair.Update(); // Draw Last
      ClearBackground(Colors.RAYWHITE);
      EndDrawing();
      HandleEvents();
    }
    return gstate;

  }

  void HandleEvents(){
    if(WindowShouldClose()){
      gstate = GameState.EXIT;
    }
    if (IsMouseButtonPressed(MouseButton.MOUSE_LEFT_BUTTON)){
      crosshair.Shoot(fowl.Fowls);
    }
    if (IsKeyPressed(KeyboardKey.KEY_R)){
      crosshair.Reload();
    }
    
  }
}

